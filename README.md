# create-skeleton

> 轻松提供 CLI 自动生成骨屏, 提供 `includeNode`, 或者 `beforeDraw` 等方法定制化骨架屏, headless 为 false 支持打开待生成骨架屏页面，手动生成骨架屏

### Install

```bash
npm i create-skeleton -D
```

### Usage

1. 方案一： 提供 `create-skeleton.config.js` , 执行`npx create-skeleton`

- 配置 `output.filePath`，如果配置的是目录，会写入到该目录里的 `create-skeleton.html` (没有的话我们会创建)文件里；
- 自定义写入的方式 `writeSkeleton: (outputHtml: string) => void;`；
- 如果前面两种方式您都没有提供，那么将会在您当前目录下创建 `create-skeleton.html` ，并将骨架屏节点写入；

2. 方案二： `npx create-skeleton <create-skeleton-url>` 生成骨架屏

### Examples

```javascript
// create-skeleton.config.js
const fs = require("fs");

module.exports = {
  url: "https://baidu.com",
  output: {
    filePath: "",
    injectSelector: "#app"
  },
  background:
    "linear-gradient(90deg,rgba(190,190,190,.2) 25%,rgba(129,129,129,.24) 37%,rgba(190,190,190,.2) 63%);background-size: 400% 100%;",
  animation: "ant-skeleton-loading 1s linear infinite;",
  header: {
    // height: 44,
    // background: "#eee"
  },
  beforeDraw: function() {
    //生成骨架屏之前的操作，比如删除干扰节点
    const conWrap = document.querySelector(".con-wrap");
    if (conWrap) {
      conWrap.style.cssText = "visibility: hidden";
    }
  },
  includeNode: function(node, draw) {
    // 删除干扰节点
    if (node.id === "skipNode") {
      return false;
    }
  }
  // writeSkeleton: function(html) {
  //   //自己处理生成的骨架屏
  //   fs.writeFileSync(__dirname, html);
  // }
};
```

### 参数说明

| 参数                                                                                                           | 说明                                                                                                                                                                            | 默认值        | 是否必填 |
| -------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------- | -------- |
| url                                                                                                            | [待生成骨架屏页面地址](https://github.com/puppeteer/puppeteer/blob/v10.4.0/docs/api.md#framegotourl-options)                                                                    | --            | 是       |
| extraHTTPHeaders                                                                                               | [请求头](https://github.com/puppeteer/puppeteer/blob/v10.4.0/docs/api.md#pagesetextrahttpheadersheaders)                                                                        | --            | 否       |
| device                                                                                                         | [设备类型](https://github.com/puppeteer/puppeteer/blob/v10.4.0/docs/api.md#puppeteerdevices)                                                                                    | iPhone 6      |          | -- | 是 |
| output.filePath                                                                                                | 生成的骨架屏节点写入的文件                                                                                                                                                      | --            | 否       |
| output.injectSelector                                                                                          | 骨架屏节点插入的位置                                                                                                                                                            | #app          | 否       |
| header.height                                                                                                  | 主题 header 的高                                                                                                                                                                | --            | 否       |
| header.background                                                                                              | 主题 header 的背景色                                                                                                                                                            | --            | 否       |
| background                                                                                                     | 骨架屏主题色                                                                                                                                                                    | #ecf0f2       | 否       |
| animation                                                                                                      | css3 动画属性                                                                                                                                                                   | --            | 否       |
| rootNode                                                                                                       | 针对模块生成骨架屏                                                                                                                                                              | document.body | 否       |
| beforeDraw                                                                                                     | 开始生成之前的操作                                                                                                                                                              | --            | 否       |
| includeNode(node, draw({width:numbenr,height:number,left:number, top:number, zIndex:number,background:'#ccc})) | 定制某个节点如何生成                                                                                                                                                            | --            | 否       |
| writeSkeleton(html)                                                                                            | 回调的骨架屏节点                                                                                                                                                                | --            | 否       |
| headless                                                                                                       | [ headless mode](https://developers.google.com/web/updates/2017/04/headless-chrome) 如果为 false, 将不关闭浏览器， 需手动点击生成骨架屏页面(按钮)右上角【骨架屏】按钮生成骨架屏 | true          | 否       |
