#!/usr/bin/env node

const path = require("path");
const cp = require("child_process");

const rootDir = path.join(__dirname, "../src");

const handleExit = () => {
  console.log("Exiting without error.");
  process.exit();
};

const handleError = e => {
  console.error("ERROR! An error was encountered while executing");
  console.error(e);
  console.log("Exiting with error.");
  process.exit(1);
};

process.on("SIGINT", handleExit);
process.on("uncaughtException", handleError);

console.log();
console.log("-------------------------------------------------------");
console.log("Assuming you have already run `npm install` to update the deps.");
console.log("If not, remember to do this before testing!");
console.log("-------------------------------------------------------");
console.log();

const createSkeletonPath = path.join(rootDir, "index.js");

cp.execSync(`node ${createSkeletonPath} `, {
  cwd: rootDir,
  stdio: "inherit"
});

// Cleanup
handleExit();
