const fs = require("fs");

module.exports = {
  headless: false,
  url: "https://baidu.com",
  output: {
    filePath: "",
    injectSelector: "#app"
  },
  background:
    "linear-gradient(90deg,rgba(190,190,190,.2) 25%,rgba(129,129,129,.24) 37%,rgba(190,190,190,.2) 63%);background-size: 400% 100%;",
  animation: "ant-skeleton-loading 1s linear infinite;",
  header: {
    // height: 44,
    // background: "#eee"
  },
  beforeDraw: function() {
    //生成骨架屏之前的操作，比如删除干扰节点
    const conWrap = document.querySelector(".con-wrap");
    if (conWrap) {
      conWrap.style.cssText = "visibility: hidden";
    }
  },
  includeNode: function(node, draw) {
    // 删除干扰节点
    if (node.id === "skipNode") {
      return false;
    }
  }
  // writeSkeleton: function(html) {
  //   //自己处理生成的骨架屏
  //   fs.writeFileSync(__dirname, html);
  // }
};
